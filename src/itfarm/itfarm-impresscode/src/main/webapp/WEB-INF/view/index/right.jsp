<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div class="col-md-4">
    <div class="col-md-12 panel panel-default" style="padding: 0;">
        <div  class="list-group">
            <a href="#" class="list-group-item "><img src="${ctx}/images/weibo_icon.png" width="30" height="30">&nbsp;&nbsp;新浪微博登陆</a>
            <a href="#" class="list-group-item"><img src="${ctx}/images/qq_icon.png" width="30" height="30">&nbsp;&nbsp;QQ账号登陆</a>
        </div>
    </div>

    <div class="col-md-12 panel panel-success" style="padding: 0;">
        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;HAVEBEEN</div>
        <div class="panel-body">
            <marquee scrollAmount=2 width=300>这是一个新手开发的代码技术分享网站，每个界面都是纯手写，使用boostrap框架</marquee>
        </div>
    </div>

    <div class="col-md-12 panel panel-default" style="padding: 0;">
        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp; 最近登录用户</div>
        <div class="panel-body">
            面板内容
        </div>
    </div>
    <div class="panel-group col-md-12" style="padding: 0" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                       aria-expanded="true" aria-controls="collapseOne">
                        <span class="glyphicon glyphicon-fire"></span>&nbsp;&nbsp; 点击排行
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in list-group" role="tabpanel"
                 aria-labelledby="headingOne">
                <c:forEach items="${animations }" var="item">
                <a href="${ctx }/article/456${item.article.recordId}.html" class="list-group-item" style="line-height: 50px; padding: 3px;">
                    <div class="col-md-4"
                         style="border-radius: 50%; width: 50px; height: 50px; background: url('${ctx}/titleImg/${item.article.titleImg}'), url('${ctx}/images/001.png'); background-size: cover">
                    </div>
                    <span style="margin-left: 20px;"><itfarm:StringCut length="35" strValue="${item.article.title}"></itfarm:StringCut></span>
                </a>
                </c:forEach>
            </div>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span class="glyphicon glyphicon-volume-up"></span>&nbsp;&nbsp;最近评论
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <c:forEach items="${comments }" var="item">
                    <div style="width:100%; height: 60px;">
                        <span style="margin-left: 20px;"><strong style="color: #6FA5DB">${item.nickName}</strong>评论了文章:<a href="${ctx }/article/456${item.parentId}.html">${item.articleTitle}</a></span>
                        <br>
                        <span style="display: block; line-height: 30px;text-indent: 30px; color: #3c8b3c">${item.content}</span>
                    </div>
                    </c:forEach>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;热搜榜
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    《风暴英雄》 是由暴雪娱乐公司开发的一款运行在Windows和Mac OS上的在线多人竞技PC游戏。
                </div>
            </div>
        </div>
        <div class="col-md-12 panel panel-default" style="padding: 0; margin-top: 20px;">
            <div class="panel-heading"><span class="glyphicon glyphicon-tag"></span>&nbsp;&nbsp;热门标签</div>
            <div class="panel-body">
                <span class="label label-success">java</span>
                <span class="label label-success">排序</span>
                <span class="label label-success">递归算法</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
                <span class="label label-success">分页</span>
            </div>
        </div>

        <div class="col-md-12 panel panel-default" style="padding: 0; margin-top: 20px;">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#dota" data-toggle="tab">
                        DotA
                    </a>
                </li>
                <li><a href="#lol" data-toggle="tab">英雄联盟</a></li>
                <li class="dropdown">
                    <a href="#" id="myTabDrop1" class="dropdown-toggle"
                       data-toggle="dropdown">其他类似游戏
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
                        <li><a href="#storm" tabindex="-1" data-toggle="tab">风暴英雄</a></li>
                        <li><a href="#h300" tabindex="-1" data-toggle="tab">300英雄</a></li>
                    </ul>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="dota">
                    <p>《DotA》（Defense of the Ancients），可以译作守护古树、守护遗迹、远古遗迹守卫， 是由暴雪公司出品即时战略游戏《魔兽争霸3》的一款多人即时对战、自定义地图，可支持10个人同时连线游戏，是暴雪公司官方认可的魔兽争霸的RPG地图。</p>
                </div>
                <div class="tab-pane fade" id="lol">
                    <p>《英雄联盟》（简称lol）是由美国Riot Games开发，中国大陆地区由腾讯游戏运营的网络游戏。</p>
                </div>
                <div class="tab-pane fade" id="storm">
                    <p>
                        《风暴英雄》 是由暴雪娱乐公司开发的一款运行在Windows和Mac OS上的在线多人竞技PC游戏。</p>
                    <p> 游戏中的英雄角色主要来自于暴雪三大经典游戏系列：《魔兽世界》、《暗黑破坏神》和《星际争霸》。它是一款道具收费的游戏，与《星际争霸Ⅱ》基于同一引擎开发。
                    </p>
                </div>
                <div class="tab-pane fade" id="h300">
                    <p>《300英雄》是由上海跳跃网络科技有限公司自主研发，深圳中青宝互动网络股份有限公司运营的一款类DOTA网游。游戏以7v7组队对抗玩法为主，提供永恒战场和永恒竞技场两种经典模式任由玩家选择，并创新性地加入勇者斗恶龙、克隆战争等多种休闲娱乐玩法。
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>