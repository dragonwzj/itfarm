package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.RolePrivilege;
import com.tc.itfarm.model.RolePrivilegeCriteria;

public interface RolePrivilegeDao extends SingleTableDao<RolePrivilege, RolePrivilegeCriteria> {
}