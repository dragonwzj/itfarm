package com.tc.itfarm.model;

import com.tc.itfarm.api.model.PageCriteria;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SystemConfigCriteria extends PageCriteria {
    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SystemConfigCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRecordIdIsNull() {
            addCriterion("record_id is null");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNotNull() {
            addCriterion("record_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecordIdEqualTo(Integer value) {
            addCriterion("record_id =", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotEqualTo(Integer value) {
            addCriterion("record_id <>", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThan(Integer value) {
            addCriterion("record_id >", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("record_id >=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThan(Integer value) {
            addCriterion("record_id <", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("record_id <=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIn(List<Integer> values) {
            addCriterion("record_id in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotIn(List<Integer> values) {
            addCriterion("record_id not in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("record_id between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("record_id not between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andWebUrlIsNull() {
            addCriterion("web_url is null");
            return (Criteria) this;
        }

        public Criteria andWebUrlIsNotNull() {
            addCriterion("web_url is not null");
            return (Criteria) this;
        }

        public Criteria andWebUrlEqualTo(String value) {
            addCriterion("web_url =", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlNotEqualTo(String value) {
            addCriterion("web_url <>", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlGreaterThan(String value) {
            addCriterion("web_url >", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlGreaterThanOrEqualTo(String value) {
            addCriterion("web_url >=", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlLessThan(String value) {
            addCriterion("web_url <", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlLessThanOrEqualTo(String value) {
            addCriterion("web_url <=", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlLike(String value) {
            addCriterion("web_url like", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlNotLike(String value) {
            addCriterion("web_url not like", value, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlIn(List<String> values) {
            addCriterion("web_url in", values, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlNotIn(List<String> values) {
            addCriterion("web_url not in", values, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlBetween(String value1, String value2) {
            addCriterion("web_url between", value1, value2, "webUrl");
            return (Criteria) this;
        }

        public Criteria andWebUrlNotBetween(String value1, String value2) {
            addCriterion("web_url not between", value1, value2, "webUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlIsNull() {
            addCriterion("admin_url is null");
            return (Criteria) this;
        }

        public Criteria andAdminUrlIsNotNull() {
            addCriterion("admin_url is not null");
            return (Criteria) this;
        }

        public Criteria andAdminUrlEqualTo(String value) {
            addCriterion("admin_url =", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlNotEqualTo(String value) {
            addCriterion("admin_url <>", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlGreaterThan(String value) {
            addCriterion("admin_url >", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlGreaterThanOrEqualTo(String value) {
            addCriterion("admin_url >=", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlLessThan(String value) {
            addCriterion("admin_url <", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlLessThanOrEqualTo(String value) {
            addCriterion("admin_url <=", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlLike(String value) {
            addCriterion("admin_url like", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlNotLike(String value) {
            addCriterion("admin_url not like", value, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlIn(List<String> values) {
            addCriterion("admin_url in", values, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlNotIn(List<String> values) {
            addCriterion("admin_url not in", values, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlBetween(String value1, String value2) {
            addCriterion("admin_url between", value1, value2, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlNotBetween(String value1, String value2) {
            addCriterion("admin_url not between", value1, value2, "adminUrl");
            return (Criteria) this;
        }

        public Criteria andWebNameIsNull() {
            addCriterion("web_name is null");
            return (Criteria) this;
        }

        public Criteria andWebNameIsNotNull() {
            addCriterion("web_name is not null");
            return (Criteria) this;
        }

        public Criteria andWebNameEqualTo(String value) {
            addCriterion("web_name =", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameNotEqualTo(String value) {
            addCriterion("web_name <>", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameGreaterThan(String value) {
            addCriterion("web_name >", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameGreaterThanOrEqualTo(String value) {
            addCriterion("web_name >=", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameLessThan(String value) {
            addCriterion("web_name <", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameLessThanOrEqualTo(String value) {
            addCriterion("web_name <=", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameLike(String value) {
            addCriterion("web_name like", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameNotLike(String value) {
            addCriterion("web_name not like", value, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameIn(List<String> values) {
            addCriterion("web_name in", values, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameNotIn(List<String> values) {
            addCriterion("web_name not in", values, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameBetween(String value1, String value2) {
            addCriterion("web_name between", value1, value2, "webName");
            return (Criteria) this;
        }

        public Criteria andWebNameNotBetween(String value1, String value2) {
            addCriterion("web_name not between", value1, value2, "webName");
            return (Criteria) this;
        }

        public Criteria andAdminNameIsNull() {
            addCriterion("admin_name is null");
            return (Criteria) this;
        }

        public Criteria andAdminNameIsNotNull() {
            addCriterion("admin_name is not null");
            return (Criteria) this;
        }

        public Criteria andAdminNameEqualTo(String value) {
            addCriterion("admin_name =", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameNotEqualTo(String value) {
            addCriterion("admin_name <>", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameGreaterThan(String value) {
            addCriterion("admin_name >", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameGreaterThanOrEqualTo(String value) {
            addCriterion("admin_name >=", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameLessThan(String value) {
            addCriterion("admin_name <", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameLessThanOrEqualTo(String value) {
            addCriterion("admin_name <=", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameLike(String value) {
            addCriterion("admin_name like", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameNotLike(String value) {
            addCriterion("admin_name not like", value, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameIn(List<String> values) {
            addCriterion("admin_name in", values, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameNotIn(List<String> values) {
            addCriterion("admin_name not in", values, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameBetween(String value1, String value2) {
            addCriterion("admin_name between", value1, value2, "adminName");
            return (Criteria) this;
        }

        public Criteria andAdminNameNotBetween(String value1, String value2) {
            addCriterion("admin_name not between", value1, value2, "adminName");
            return (Criteria) this;
        }

        public Criteria andWebTitleIsNull() {
            addCriterion("web_title is null");
            return (Criteria) this;
        }

        public Criteria andWebTitleIsNotNull() {
            addCriterion("web_title is not null");
            return (Criteria) this;
        }

        public Criteria andWebTitleEqualTo(String value) {
            addCriterion("web_title =", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleNotEqualTo(String value) {
            addCriterion("web_title <>", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleGreaterThan(String value) {
            addCriterion("web_title >", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleGreaterThanOrEqualTo(String value) {
            addCriterion("web_title >=", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleLessThan(String value) {
            addCriterion("web_title <", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleLessThanOrEqualTo(String value) {
            addCriterion("web_title <=", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleLike(String value) {
            addCriterion("web_title like", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleNotLike(String value) {
            addCriterion("web_title not like", value, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleIn(List<String> values) {
            addCriterion("web_title in", values, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleNotIn(List<String> values) {
            addCriterion("web_title not in", values, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleBetween(String value1, String value2) {
            addCriterion("web_title between", value1, value2, "webTitle");
            return (Criteria) this;
        }

        public Criteria andWebTitleNotBetween(String value1, String value2) {
            addCriterion("web_title not between", value1, value2, "webTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleIsNull() {
            addCriterion("admin_title is null");
            return (Criteria) this;
        }

        public Criteria andAdminTitleIsNotNull() {
            addCriterion("admin_title is not null");
            return (Criteria) this;
        }

        public Criteria andAdminTitleEqualTo(String value) {
            addCriterion("admin_title =", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleNotEqualTo(String value) {
            addCriterion("admin_title <>", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleGreaterThan(String value) {
            addCriterion("admin_title >", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleGreaterThanOrEqualTo(String value) {
            addCriterion("admin_title >=", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleLessThan(String value) {
            addCriterion("admin_title <", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleLessThanOrEqualTo(String value) {
            addCriterion("admin_title <=", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleLike(String value) {
            addCriterion("admin_title like", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleNotLike(String value) {
            addCriterion("admin_title not like", value, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleIn(List<String> values) {
            addCriterion("admin_title in", values, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleNotIn(List<String> values) {
            addCriterion("admin_title not in", values, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleBetween(String value1, String value2) {
            addCriterion("admin_title between", value1, value2, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleNotBetween(String value1, String value2) {
            addCriterion("admin_title not between", value1, value2, "adminTitle");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountIsNull() {
            addCriterion("web_menu_count is null");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountIsNotNull() {
            addCriterion("web_menu_count is not null");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountEqualTo(Integer value) {
            addCriterion("web_menu_count =", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountNotEqualTo(Integer value) {
            addCriterion("web_menu_count <>", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountGreaterThan(Integer value) {
            addCriterion("web_menu_count >", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("web_menu_count >=", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountLessThan(Integer value) {
            addCriterion("web_menu_count <", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountLessThanOrEqualTo(Integer value) {
            addCriterion("web_menu_count <=", value, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountIn(List<Integer> values) {
            addCriterion("web_menu_count in", values, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountNotIn(List<Integer> values) {
            addCriterion("web_menu_count not in", values, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountBetween(Integer value1, Integer value2) {
            addCriterion("web_menu_count between", value1, value2, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andWebMenuCountNotBetween(Integer value1, Integer value2) {
            addCriterion("web_menu_count not between", value1, value2, "webMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountIsNull() {
            addCriterion("admin_menu_count is null");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountIsNotNull() {
            addCriterion("admin_menu_count is not null");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountEqualTo(Integer value) {
            addCriterion("admin_menu_count =", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountNotEqualTo(Integer value) {
            addCriterion("admin_menu_count <>", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountGreaterThan(Integer value) {
            addCriterion("admin_menu_count >", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("admin_menu_count >=", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountLessThan(Integer value) {
            addCriterion("admin_menu_count <", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountLessThanOrEqualTo(Integer value) {
            addCriterion("admin_menu_count <=", value, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountIn(List<Integer> values) {
            addCriterion("admin_menu_count in", values, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountNotIn(List<Integer> values) {
            addCriterion("admin_menu_count not in", values, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountBetween(Integer value1, Integer value2) {
            addCriterion("admin_menu_count between", value1, value2, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andAdminMenuCountNotBetween(Integer value1, Integer value2) {
            addCriterion("admin_menu_count not between", value1, value2, "adminMenuCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountIsNull() {
            addCriterion("article_count is null");
            return (Criteria) this;
        }

        public Criteria andArticleCountIsNotNull() {
            addCriterion("article_count is not null");
            return (Criteria) this;
        }

        public Criteria andArticleCountEqualTo(Integer value) {
            addCriterion("article_count =", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountNotEqualTo(Integer value) {
            addCriterion("article_count <>", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountGreaterThan(Integer value) {
            addCriterion("article_count >", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("article_count >=", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountLessThan(Integer value) {
            addCriterion("article_count <", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountLessThanOrEqualTo(Integer value) {
            addCriterion("article_count <=", value, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountIn(List<Integer> values) {
            addCriterion("article_count in", values, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountNotIn(List<Integer> values) {
            addCriterion("article_count not in", values, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountBetween(Integer value1, Integer value2) {
            addCriterion("article_count between", value1, value2, "articleCount");
            return (Criteria) this;
        }

        public Criteria andArticleCountNotBetween(Integer value1, Integer value2) {
            addCriterion("article_count not between", value1, value2, "articleCount");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNull() {
            addCriterion("language is null");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNotNull() {
            addCriterion("language is not null");
            return (Criteria) this;
        }

        public Criteria andLanguageEqualTo(String value) {
            addCriterion("language =", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotEqualTo(String value) {
            addCriterion("language <>", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThan(String value) {
            addCriterion("language >", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("language >=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThan(String value) {
            addCriterion("language <", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThanOrEqualTo(String value) {
            addCriterion("language <=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLike(String value) {
            addCriterion("language like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotLike(String value) {
            addCriterion("language not like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageIn(List<String> values) {
            addCriterion("language in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotIn(List<String> values) {
            addCriterion("language not in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageBetween(String value1, String value2) {
            addCriterion("language between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotBetween(String value1, String value2) {
            addCriterion("language not between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andEncodingIsNull() {
            addCriterion("encoding is null");
            return (Criteria) this;
        }

        public Criteria andEncodingIsNotNull() {
            addCriterion("encoding is not null");
            return (Criteria) this;
        }

        public Criteria andEncodingEqualTo(String value) {
            addCriterion("encoding =", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingNotEqualTo(String value) {
            addCriterion("encoding <>", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingGreaterThan(String value) {
            addCriterion("encoding >", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingGreaterThanOrEqualTo(String value) {
            addCriterion("encoding >=", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingLessThan(String value) {
            addCriterion("encoding <", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingLessThanOrEqualTo(String value) {
            addCriterion("encoding <=", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingLike(String value) {
            addCriterion("encoding like", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingNotLike(String value) {
            addCriterion("encoding not like", value, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingIn(List<String> values) {
            addCriterion("encoding in", values, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingNotIn(List<String> values) {
            addCriterion("encoding not in", values, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingBetween(String value1, String value2) {
            addCriterion("encoding between", value1, value2, "encoding");
            return (Criteria) this;
        }

        public Criteria andEncodingNotBetween(String value1, String value2) {
            addCriterion("encoding not between", value1, value2, "encoding");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserIsNull() {
            addCriterion("last_modify_user is null");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserIsNotNull() {
            addCriterion("last_modify_user is not null");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserEqualTo(String value) {
            addCriterion("last_modify_user =", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserNotEqualTo(String value) {
            addCriterion("last_modify_user <>", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserGreaterThan(String value) {
            addCriterion("last_modify_user >", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserGreaterThanOrEqualTo(String value) {
            addCriterion("last_modify_user >=", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserLessThan(String value) {
            addCriterion("last_modify_user <", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserLessThanOrEqualTo(String value) {
            addCriterion("last_modify_user <=", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserLike(String value) {
            addCriterion("last_modify_user like", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserNotLike(String value) {
            addCriterion("last_modify_user not like", value, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserIn(List<String> values) {
            addCriterion("last_modify_user in", values, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserNotIn(List<String> values) {
            addCriterion("last_modify_user not in", values, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserBetween(String value1, String value2) {
            addCriterion("last_modify_user between", value1, value2, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserNotBetween(String value1, String value2) {
            addCriterion("last_modify_user not between", value1, value2, "lastModifyUser");
            return (Criteria) this;
        }

        public Criteria andWebUrlLikeInsensitive(String value) {
            addCriterion("upper(web_url) like", value.toUpperCase(), "webUrl");
            return (Criteria) this;
        }

        public Criteria andAdminUrlLikeInsensitive(String value) {
            addCriterion("upper(admin_url) like", value.toUpperCase(), "adminUrl");
            return (Criteria) this;
        }

        public Criteria andWebNameLikeInsensitive(String value) {
            addCriterion("upper(web_name) like", value.toUpperCase(), "webName");
            return (Criteria) this;
        }

        public Criteria andAdminNameLikeInsensitive(String value) {
            addCriterion("upper(admin_name) like", value.toUpperCase(), "adminName");
            return (Criteria) this;
        }

        public Criteria andWebTitleLikeInsensitive(String value) {
            addCriterion("upper(web_title) like", value.toUpperCase(), "webTitle");
            return (Criteria) this;
        }

        public Criteria andAdminTitleLikeInsensitive(String value) {
            addCriterion("upper(admin_title) like", value.toUpperCase(), "adminTitle");
            return (Criteria) this;
        }

        public Criteria andLanguageLikeInsensitive(String value) {
            addCriterion("upper(language) like", value.toUpperCase(), "language");
            return (Criteria) this;
        }

        public Criteria andEncodingLikeInsensitive(String value) {
            addCriterion("upper(encoding) like", value.toUpperCase(), "encoding");
            return (Criteria) this;
        }

        public Criteria andLastModifyUserLikeInsensitive(String value) {
            addCriterion("upper(last_modify_user) like", value.toUpperCase(), "lastModifyUser");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}