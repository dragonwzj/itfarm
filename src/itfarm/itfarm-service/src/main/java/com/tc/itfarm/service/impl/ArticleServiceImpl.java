package com.tc.itfarm.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.BeanMapper;
import com.tc.itfarm.api.util.CollectionUtil;
import com.tc.itfarm.model.Category;
import com.tc.itfarm.model.MenuArctile;
import com.tc.itfarm.service.CategoryService;
import com.tc.itfarm.service.MenuArticleService;
import com.tc.itfarm.service.MenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.PageQueryHelper;
import com.tc.itfarm.dao.ArticleDao;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ArticleCriteria;
import com.tc.itfarm.model.ext.ArticleEx;
import com.tc.itfarm.service.ArticleService;
import org.springframework.util.Assert;

@Service
public class ArticleServiceImpl extends BaseServiceImpl<Article> implements ArticleService {

	@Resource
	private ArticleDao articleDao;
	@Resource
	private MenuArticleService menuArticleService;
	@Resource
	private CategoryService categoryService;

	@Override
	public PageList<Article> selectArticleByPage(Page page, Integer categoryId, String title) {
		ArticleCriteria criteria = new ArticleCriteria();
		ArticleCriteria.Criteria innerCriteria = criteria.createCriteria();
		if (categoryId != null && categoryId != 0) {
			innerCriteria.andTypeIdEqualTo(categoryId);
		}
		if (StringUtils.isNotBlank(title)) {
			innerCriteria.andTitleLike("%"+title+"%");
		}
		return PageQueryHelper.queryPage(page, criteria, articleDao, QueryArticleEnum.TITLE.criteria);
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return articleDao;
	}

	@Override
	public void save(Article article, Integer menuId) {
		if (article.getRecordId() == null) {
			article.setCreateTime(DateUtils.now());
			article.setModifyTime(DateUtils.now());
			article.setOrderNo(0);
			articleDao.insert(article);
			MenuArctile menuArctile = new MenuArctile();
			menuArctile.setMenuId(menuId);
			menuArctile.setArticleId(article.getRecordId());
			menuArctile.setArticleTitle(article.getTitle());
			menuArctile.setCreateTime(DateUtils.now());
			menuArticleService.insert(menuArctile);
		} else {
			article.setModifyTime(DateUtils.now());
			articleDao.updateByIdSelective(article);
		}
	}

	@Override
	public List<Article> selectArticles(QueryArticleEnum e, int count) {
		ArticleCriteria criteria = new ArticleCriteria();
		Page page = new Page(0, count);
		criteria.setPage(page);
		criteria.setOrderByClause(e.getCriteria());
		return articleDao.selectByCriteria(criteria);
	}

	@Override
	public ArticleEx selectLast(String title) {
		return articleDao.selectLast(title);
	}

	@Override
	public ArticleEx selectNext(String title) {
		return articleDao.selectNext(title);
	}

	@Override
	public void modifyToCategory(Integer categoryId, List<Integer> ids) {
		Assert.notNull(categoryId, "分类id不能为空!");
		Assert.notNull(ids, "要移动的文章id集合不能为空！");
		Assert.isTrue(ids.size() > 0, "请选择要移动的文章!");
		Map<String, Object> map = Maps.newHashMap();
		map.put("desId", categoryId);
		map.put("ids", ids);
		articleDao.changeCategory(map);
	}

	@Override
	public Integer delete(Integer id) {
		menuArticleService.deleteByArticleId(id);
		return super.delete(id);
	}

	@Override
	public void delete(List<Integer> ids) {
		for (Integer id : ids) {
			menuArticleService.deleteByArticleId(id);
			articleDao.deleteById(id);
		}
	}

	@Override
	public List<Article> selectByIds(List<Integer> ids) {
		if (ids.size() < 1) {
			return Lists.newArrayList();
		}
		ArticleCriteria criteria = new ArticleCriteria();
		criteria.or().andRecordIdIn(ids);
		return articleDao.selectByCriteria(criteria);
	}

	@Override
	public void updatePageView(Integer recordId, Integer pageView) {
		articleDao.addPageView(recordId, pageView);
	}

	@Override
	public PageList<Article> selectCodeArticleByPage(Page page, String title) {
		ArticleCriteria criteria = new ArticleCriteria();
		ArticleCriteria.Criteria innerCriteria = criteria.createCriteria();
		List<Category> categories = categoryService.selectAll((short) 1);
		List<Integer> ids = BeanMapper.copyPropertyToList(categories, Integer.class, "recordId");
		innerCriteria.andTypeIdIn(ids);
		if (StringUtils.isNotBlank(title)) {
			innerCriteria.andTitleLike("%"+title+"%");
		}
		return PageQueryHelper.queryPage(page, criteria, articleDao, QueryArticleEnum.CREATE_TIME.criteria);
	}
}
